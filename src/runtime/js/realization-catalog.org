#+TITLE: Realization Catalog of Polynomial arithmetic using Linked list 
#+AUTHOR: VLEAD
#+DATE: 
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document captures the realization catalog of polynomial arithmetic using linked list
experiment.

* Realization Catalog
#+NAME: realization-catalog
#+BEGIN_SRC js

var realizationCatalog = {
  "Pictorial representation": {
    "resource_type": "Image",
    "url": "/build/code/static/images/linkedlist-singly-doubly-circular.jpg",
    "height":"80%",
    "width":"100%"
  },

  "Representation of Polynomials": {
    "resource_type": "Image",
    "url": "/build/code/static/images/Representation of polynomials using linkedlist.jpg",
    "height":"80%",
    "width":"100%"
  }, 

  "Polynomial Addition":{
    "resource_type": "Image",
    "url": "/build/code/static/images/Polynomials-additions.jpg",
    "height":"80%",
    "width":"75%"
  },

  "Polynomial Subtraction": {
    "resource_type": "Image",
    "url": "/build/code/static/images/Polynomials-substration.jpg",
    "height":"60%",
    "width":"75%"
  },

  "Polynomial Multiplication": {
    "resource_type": "Image",
    "url": "/build/code/static/images/Polynomials-multiplication.jpg",
    "height":"60%",
    "width":"75%"
  },

  "Comparison with other Algorithms": {
    "resource_type": "Image",
    "url": "/build/code/static/images/time-complexity.jpg",
    "height":"60%",
    "width":"75%"
  },

  "Demonstration of Polynomials:Linked list": {
    "reqs_satisfied": "Provides an artefact to Polynomials",
    "resource_type": "html",
    "url": "/build/code/runtime/html/demo.html"
  },

  "Practice of Polynomials:Linked list": {
    "reqs_satisfied": "Provides an artefact to Polynomials",
    "resource_type": "html",
    "url": "/build/code/runtime/html/add-practice.html"
  },

  "Exercise of Polynomials:Linked list": {
    "reqs_satisfied": "artefact",
    "resource_type": "html",
    "url": "/build/code/runtime/html/add-exercise.html"
  },

  "Experiment Introduction": {
    "resource_type" : "video",
    "url": "https://www.youtube.com/embed/Bb6TsOG2bqQ"
  },

  "Basic Concept": {
    "resource_type" : "video",
    "url": "https://www.youtube.com/embed/AzKFUdf-1Pk"
  },

  "Polynomial using Linked list Algorithm": {
    "resource_type" : "video",
    "url": "https://www.youtube.com/embed/EH80MJU-VBM"
  }     
};

#+END_SRC

* Tangle
#+BEGIN_SRC javascript :tangle realization.js :eval no :noweb yes
<<realization-catalog>>
#+END_SRC
