#+TITLE: Exercise artefact for Polynomial arithmetic using Linked list
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Polynomial arithmetic using Linked list exercise=
  interactive artefact(HTML).

* Features of the artefact
  + Artefact tests user's understanding of polynomial arithmetic using linked list
  + Artefact generates a question
  + User can run iterations by clicking on =run iteration=
    button
  + User can use the artefact to solve the question
  + User will enter answer value in =textbox= provided
  + User can =submit= answer and get feedback(right/wrong)

* Head elements
#+NAME: head-elements
#+BEGIN_SRC html
<!DOCTYPE html>
<html>
<head>
  <title>Exercise</title>
  <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
  <link rel="stylesheet" type="text/css" href="../css/demo.css">

<style>
  .card {
    display: inline-block;
    font-size: 1.5vw;
    padding: 1.2%;
    margin: 0.05%;  
    color: white;
    background-color: #288ec8;
}
  .arrow {
  position: relative;
  background-color: #000;
  width: 35%;
  height: 3px;
  top:17px;
  left:-21px;
}

  .arrow::after {
  content: '';
  position: absolute;
  width: 35%;
  height: 3px;
  top: -3px;
  right: -3px;
  background-color: #000;
  transform: rotate(45deg);
}
  .arrow::before {
  content: '';
  position: absolute;
  width: 35%;
  height: 3px;
  top: 3px;
  right: -3px;
  background-color: #000;
  transform: rotate(-45deg);
}
  .icon {
    position: relative;
    width: 80px;
    cursor: pointer;
    display: inline-block;
    font-size: 1.5vw;
    top: -23px;
    left: 22px;
}
</style>
</head>
#+END_SRC

* Instruction Box
#+NAME: instruction-box
#+BEGIN_SRC html
<body>
    <div class="instruction-box">
      <button class="collapsible">Instructions</button>
      <div class="content">
        <ul>
          <li>Now you have to perform addition of two polynomials using Linked list</li>
          <li>Please follow descending order in the powers while giving inputs</li>
          <li>Click on the <b>Submit</b> button to submit</li>
          <li>Click on the <b>Reset</b> button to reset restart the Exercise</li>
        </ul>
      </div>
    </div>
      <br>
      <br>
#+END_SRC

* Generating equations
#+NAME: generating-equations
#+BEGIN_SRC html
    <div id="polynomial-eqs">
        <div id="expr1" align="center"></div><br/> <br>
        <div id="expr2" align="center"></div><br/> <br>
        <div id="card" ></div>
        <div id="arrow"></div>
    </div>    
#+END_SRC

* Equations
#+NAME: equations
#+BEGIN_SRC html   
        <div class="btn-wrapper" align="center">
        R(x) = <input type="numbers" id="value1" size="1" value="" pattern="9"/> x ^
            <input type="numbers" id="valuea" size="1" value="" style="width:25px;" pattern="4" />  + 
            <input type="numbers" id="value2" size="1" value="" pattern="13" /> x ^ 
            <input type="numbers" id="valueb" size="1" value="" style="width:25px;" pattern="3" /> +
            <input type="numbers" id="value3" size="1" value="" pattern="3" /> x ^
            <input type="numbers" id="valuec" size="1" value="" style="width:25px;" pattern="2" />  + 
            <input type="numbers" id="value4" size="1" value="" pattern="6" /> x ^ 
            <input type="numbers" id="valued" size="1" value="" style="width:25px;" pattern="1" /> +
            <input type="numbers" id="value5" size="1" value="" pattern="9" /> x ^
            <input type="numbers" id="valuee" size="1" value="" pattern="0" style="width:25px;"/>
            <br>
            <br>
            <br>
        </div>
#+END_SRC

* Solutions
#+NAME: solution
#+BEGIN_SRC html 
      <div>
        <div id="solution" align="center"></div>
      </div>
      <br>
#+END_SRC

* Observations
#+NAME: comments
#+BEGIN_SRC html
      <div class="comment-box" id="comment-box-bigger" align="center" style="padding-left:265px;">
        <b>Observations</b>
        <p id="ins"></p>
      </div>
#+END_SRC

* Buttons for Start and Pause
#+NAME: buttons
#+BEGIN_SRC html 
      <div class="buttons-wrapper" align="center">
        <input class="button-input" type="button" value="Submit" id="start">
        <input class="button-input" type="button" value="Reset" id="reset">
      </div> 
    </div>
#+END_SRC

* Links and endine tags
#+NAME: links-tags
#+BEGIN_SRC html
    <script src="../js/add-exercise.js"></script>
    <script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
    </body>
</html>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle add-exercise.html :eval no :noweb yes
<<head-elements>>
<<instruction-box>>
<<generating-equations>>
<<equations>>
<<solution>>
<<comments>>
<<buttons>>
<<links-tags>>
#+END_SRC
