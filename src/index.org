#+TITLE:  Index to Artefacts and Realization Catalog
#+AUTHOR: VLEAD
#+DATE: [2018-12-05 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introductions:
  This is an index to implementation of artefacts and
  realization catalog.

* Number of artefacts : 4
+ 2 demo artefacts
+ 1 practice artefacts
+ 1 exercise artefacts

* External pictures/videos used : None
* References
** A mapping between different implementations and their roles.
    |------+-----------------------------------+-------------------------------------------------------|
    | S.No | Artefacts and Realization Catalog | Role                                                  |
    |------+-----------------------------------+-------------------------------------------------------|
    |    1 | [[./runtime/html/selection_demo.org][selectionsort-demo]]                | Holds the demo for selection sort                     |
    |------+-----------------------------------+-------------------------------------------------------|
    |    2 | [[./runtime/html/selection_exercise.org][selectionsort-exercise]]            | Holds the exercise for selection sort                 |
    |------+-----------------------------------+-------------------------------------------------------|
    |    3 | [[./runtime/html/selection_practice.org][selectionsort-practice]]            | Holds the practice artefact for selection sort        |
    |------+-----------------------------------+-------------------------------------------------------|
    |    4 | [[./runtime/html/selection_time.org][time complexity]]                   | Holds the time complexity artefact for selection sort |
    |------+-----------------------------------+-------------------------------------------------------|
    |    5 | [[./runtime/realization-catalog.org][realization catalog]]               | Holds the realization catalog                         |
    |------+-----------------------------------+-------------------------------------------------------|

** Link to the videos uploaded on youtube : [[https://www.youtube.com/playlist?list=PLEX8ypAOJNJdeSPdgGgZyo8xj6vb1PLax][Selection Sort Videos]]

